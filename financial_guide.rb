#!/usr/bin/env ruby

require_relative 'lib/roman_numeral'
require_relative 'lib/alien_parser'

if ARGV.empty?
	puts "Please inform a file to read"
else

	filename = ARGV[0].sub(/^\./, "./alien_annotations.txt")

	@alien = AlienParser.new
	File.open(filename, "r").each_line do |line|
		@alien.add_sentence line

		result = @alien.evaluate

		puts result if result

	end
end