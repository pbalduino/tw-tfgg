class AlienParser
  TOKEN_DEFINITION =  /^\w+ is [IVXLCDM]$/

  VALUE_DEFINITION = /^(\w+ )+(Silver|Gold|Iron) is \d+ Credits$/

  QUESTION_RESOLUTION = /^how (much|many Credits)? is (\w+ )+(Silver |Gold |Iron )?\?$/

  NO_IDEA = "I have no idea what you are talking about"

  def initialize
    @sentences = []
    @scope = {}
    @roman_parser =  RomanNumeralConverter.new
  end

  def add_sentence(sentence)
    @sentences << sentence
  end

  def evaluate
  	result = NO_IDEA

    @sentences.each do |sentence|

      if sentence =~ TOKEN_DEFINITION
      	define_token sentence
      	result = nil

      elsif sentence =~ VALUE_DEFINITION
      	attribute_value sentence
      	result = nil

      elsif sentence =~ QUESTION_RESOLUTION
      	result = answer_question(sentence)

      else
      	result = NO_IDEA

      end

    end

    result
      		
  end

  private 

  def define_token(sentence)
		alien_word = /^\w+/.match(sentence)[0]

		roman_numeral = /^[IVXLCDM]/.match(sentence.sub(/^\w+ is /, ""))[0]

		@scope[alien_word.to_sym] = {:roman => roman_numeral}

  end

  def answer_question(sentence)
  	sentence.sub! /^how /, ""

  	if sentence =~ /^much/
  		sentence.sub! /^much is /, ""

  		alien_numerals = consume(sentence, /^\w+ /)

  		begin
  			value = alien_to_number alien_numerals
	  		
  			text = alien_numerals.reduce{|item, sum| item += " #{sum}"}

  			"#{text} is #{value}"

			rescue Exception
				NO_IDEA
			end

		else
			sentence.sub! /^many Credits is /, ""

			material = /(Silver|Gold|Iron)/.match(sentence)[0]

			sentence.sub! /(Silver|Gold|Iron) \?/, ""

			alien_numerals = consume(sentence, /^\w+ /) 

			number = alien_to_number(alien_numerals)

			"#{alien_numerals.reduce{|item, sum| item += " #{sum}"}} #{material} is #{(@scope[material.to_sym] * number).to_i} Credits"
		end
  end

  def alien_to_number(alien_numerals)
		roman = alien_numerals.map{|numeral| @scope[numeral.to_sym][:roman]}.reduce(:+)

		value = @roman_parser.convert roman
  end

  def consume(string, regex, array = [])
  	if(string =~ regex)
  		array << regex.match(string)[0].strip

			consume(string.sub(regex, ""), regex, array)
  	else
  		array
  	end
  end

  def attribute_value(sentence)
  	base = /^(\w+ )+(Silver|Gold|Iron)/.match(sentence)[0]

  	material = /(Silver|Gold|Iron)/.match(base)[0]

  	alien_numerals = consume(base.sub(material, ""), /\w+ /)

  	quantity = alien_to_number alien_numerals

  	credits = /\d+/.match(sentence.sub(/^(\w+ )+(Silver|Gold|Iron)/, ""))[0]

  	@scope[material.to_sym] = credits.to_f / quantity.to_f

  end

end