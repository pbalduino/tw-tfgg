class RomanNumeralConverter
  NONE = {:value => 0,
          :valid_states => [],
          :regex => /^$/}

  I = {:value => 1,
         :valid_states => [NONE],
         :regex => /^I/}

  II = {:value => 2,
         :valid_states => [NONE],
         :regex => /^II/}

  III = {:value => 3,
         :valid_states => [NONE],
         :regex => /^III/}

  IV = {:value => 4,
         :valid_states => [NONE],
         :regex => /^IV/}

  V = {:value => 5,
         :valid_states => [III, II, I, NONE],
         :regex => /^V/}

  IX = {:value => 9,
         :valid_states => [NONE],
         :regex => /^IX/}

  X = {:value => 10,
         :valid_states => [IX, IV, V, III, II, I, NONE],
         :regex => /^X/}

  XX = {:value => 20,
         :valid_states => [IX, IV, V, III, II, I, NONE],
         :regex => /^XX/}

  XXX = {:value => 30,
         :valid_states => [IX, IV, V, III, II, I, NONE],
         :regex => /^XXX/}

  XL = {:value => 40,
         :valid_states => [IX, IV, V, III, II, I, NONE],
         :regex => /^XL/}

  L = {:value => 50,
         :valid_states => [XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^L/}

  XC = {:value => 90,
         :valid_states => [IX, IV, V, III, II, I, NONE],
         :regex => /^XC/}

  C = {:value => 100,
         :valid_states => [XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^C/}

  CC = {:value => 200,
         :valid_states => [XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^CC/}

  CCC = {:value => 300,
         :valid_states => [XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^CCC/}

  CD = {:value => 400,
         :valid_states => [XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^CD/}

  D = {:value => 500,
         :valid_states => [CCC, CC, C, XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^D/}

  CM = {:value => 900,
         :valid_states => [XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^CM/}

  M = {:value => 1000,
         :valid_states => [CM, D, CD, CCC, CC, C, XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^M/}

  MM = {:value => 2000,
         :valid_states => [CM, D, CD, CCC, CC, C, XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^MM/}

  MMM = {:value => 3000,
         :valid_states => [CM, D, CD, CCC, CC, C, XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE],
         :regex => /^MMM/}

  INITIAL_VALID_STATE = [MMM, MM, M, CM, D, CD, CCC, CC, C, XC, L, XL, XXX, XX, X, IX, IV, V, III, II, I, NONE]

  def convert(roman)
    total = calculate_value(roman)

  end

private
  def calculate_value(roman, valid_states = INITIAL_VALID_STATE)
    sum = 0
    valid = false

    valid_states.each do |state|
      if roman =~ state[:regex]

        valid = true
        rest = roman.sub(state[:regex], "")

        sum = state[:value]

        sum += calculate_value(rest, state[:valid_states]) unless rest.empty?

        break
      end
    end 

    raise Exception.new("Invalid roman numeral - #{roman}") unless valid

    sum

  end
end