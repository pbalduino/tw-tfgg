Author: Plínio Balduino - pbalduino@gmail.com

Installation:

- To run this application you will need:
	- Ruby 1.9.3
	- RSpec
	- SimpleCov
	- After install Ruby, you can run 'gem install rspec simplecov' (without quotes) to install everything we will need

Running:

- If you're running Linux, MacOS X or any flavour of Unix, you can run the following steps:

  chmod +x financial_guide.rb

  ./financial_guide.rb alien_annotations.txt

  If you don't have permission to run chmod, you can just run as follows:

  ruby financial_guide.rb alien_annotations.txt

- If you're running Windows, run the command:

  financial_guide.bat alien_annotations.txt

Note: The file alien_annotations.txt contains some annotations about intergalactic transactions, as explained below.

Design:

The Roman Numerals follow clear and simple rules, however the code written using IFs soon turned a mess and it was not a good option. Drawing some possibilities I decided to handle the numerals using a state machine. The first part of RomanNumeralConverter is a declaration of valid states in every situation. The second part is a generic loop that uses recursion to consume the numeral until it ends. If invalid character or combination is found, the state machine abort the process, raising a generic exception.

After I have the roman numeral handler working for any situation, I decided to develop the Alien Translator (that I after renamed Alien Parser) using the same principle of separated states and a generic recursive function to handle everything.

Again, soon the code became messed and hard to read. So I divided the alien annotations in three main groups, which are selected based on common patterns defined as simples regular expressions.
- Token declaration, where an alien word is associated with a roman numeral;
- Value definition, where the value of each material is defined in alien terms. Here we could only translate the alien expression to readable numbers, if needed; 
- Question resolution, where the user finally can see the alien expressions translated to earthling text.

An expression that doesn't fit any of these three cases is automatically considered invalid.

This way, I could create smaller and specialized state machines to solve the problem, relying on roman numeral validation to handle most of invalid alien expression.

The problem:

MERCHANT'S GUIDE TO THE GALAXY

You decided to give up on earth after the latest financial collapse left 99.99% of the earth's population with 0.01% of the wealth. Luckily, with the scant sum of money that is left in your account, you are able to afford to rent a spaceship, leave earth, and fly all over the galaxy to sell common metals and dirt (which apparently is worth a lot).

Buying and selling over the galaxy requires you to convert numbers and units, and you decided to write a program to help you.

The numbers used for intergalactic transactions follows similar convention to the roman numerals and you have painstakingly collected the appropriate translation between them.

Roman numerals are based on seven symbols:
Symbol
Value
I	1
V	5
X	10
L	50
C	100
D	500
M	1,000


Numbers are formed by combining symbols together and adding the values. For example, MMVI is 1000 + 1000 + 5 + 1 = 2006. Generally, symbols are placed in order of value, starting with the largest values. When smaller values precede larger values, the smaller values are subtracted from the larger values, and the result is added to the total. For example MCMXLIV = 1000 + (1000 − 100) + (50 − 10) + (5 − 1) = 1944.

* The symbols "I", "X", "C", and "M" can be repeated three times in succession, but no more. (They may appear four times if the third and fourth are separated by a smaller value, such as XXXIX.) "D", "L", and "V" can never be repeated.
* "I" can be subtracted from "V" and "X" only. "X" can be subtracted from "L" and "C" only. "C" can be subtracted from "D" and "M" only. "V", "L", and "D" can never be subtracted.
* Only one small-value symbol may be subtracted from any large-value symbol.
* A number written in Arabic numerals can be broken into digits. For example, 1903 is composed of 1, 9, 0, and 3. To write the Roman numeral, each of the non-zero digits should be treated separately. In the above example, 1,000 = M, 900 = CM, and 3 = III. Therefore, 1903 = MCMIII.

-- Source: Wikipedia (http://en.wikipedia.org/wiki/Roman_numerals)


Input to your program consists of lines of text detailing your notes on the conversion between intergalactic units and roman numerals. 

You are expected to handle invalid queries appropriately.

Test input:
glob is I
prok is V
pish is X
tegj is L
glob glob Silver is 34 Credits
glob prok Gold is 57800 Credits
pish pish Iron is 3910 Credits
how much is pish tegj glob glob ?
how many Credits is glob prok Silver ?
how many Credits is glob prok Gold ?
how many Credits is glob prok Iron ?
how much wood could a woodchuck chuck if a woodchuck could chuck wood ?

Test Output:
pish tegj glob glob is 42
glob prok Silver is 68 Credits
glob prok Gold is 57800 Credits
glob prok Iron is 782 Credits
I have no idea what you are talking about
