require_relative 'spec_helper'

describe "Roman numerals" do

  before :each do 
    @roman_converter = RomanNumeralConverter.new
  end

  it "III should be 3" do
    @roman_converter.convert("III").should == 3
  end

  it "IIII should be invalid" do
    lambda{@roman_converter.convert("IIII")}.should raise_exception
  end

  it "V should be 5" do
    @roman_converter.convert("V").should == 5
  end

  it "VIII should be 8" do
    @roman_converter.convert("VIII").should == 8
  end

  it "VII should be 7" do
    @roman_converter.convert("VII").should == 7
  end

  it "VI should be 6" do
    @roman_converter.convert("VI").should == 6
  end

  it "IV should be 4" do
    @roman_converter.convert("IV").should == 4
  end

  it "IVI should be invalid" do
    lambda{@roman_converter.convert("IVI")}.should raise_exception
  end

  it "IX should be 9" do
    @roman_converter.convert("IX").should == 9
  end

  it "X should be 10" do
    @roman_converter.convert("X").should == 10
  end

  it "X should be 10" do
    @roman_converter.convert("X").should == 10
  end

  it "XX should be 20" do
    @roman_converter.convert("XX").should == 20
  end

  it "XXX should be 30" do
    @roman_converter.convert("XXX").should == 30
  end

  it "LX should be 60" do
    @roman_converter.convert("LX").should == 60
  end

  it "XXXVIII should be 38" do
    @roman_converter.convert("XXXVIII").should == 38
  end

  it "XL should be 40" do
    @roman_converter.convert("XL").should == 40
  end

  it "XC should be 90" do
    @roman_converter.convert("XC").should == 90
  end

  it "MCMLXXXIII" do
    @roman_converter.convert("MCMLXXXIII").should == 1983
  end

  it "MCCMLXXXIII" do
    lambda{@roman_converter.convert("MCCMLXXXIII")}.should raise_exception
  end

  it "MMMDCCCLXXXVIII should be 3888" do
    @roman_converter.convert("MMMDCCCLXXXVIII").should == 3888
  end
end