require_relative 'spec_helper'

describe "Alien parser" do

  describe "Sintax definition" do
    it "'glob is I' is a valid token definition" do
      ("glob is I" =~ AlienParser::TOKEN_DEFINITION).should be_true
    end

    it "'prok is V' is a valid token definition" do
      ("prok is V" =~ AlienParser::TOKEN_DEFINITION).should be_true
    end

    it "'pish is X' is a valid token definition" do
      ("pish is X" =~ AlienParser::TOKEN_DEFINITION).should be_true
    end

    it "'tegj is L' is a valid token definition" do
      ("tegj is L" =~ AlienParser::TOKEN_DEFINITION).should be_true
    end

    it "'glob glob Silver is 34 Credits' is a valid value definition" do
      ("glob glob Silver is 34 Credits" =~ AlienParser::VALUE_DEFINITION).should be_true
    end

    it "'glob prok Gold is 57800 Credits' is a valid value definition" do
      ("glob prok Gold is 57800 Credits" =~ AlienParser::VALUE_DEFINITION).should be_true
    end
    
    it "'pish pish Iron is 3910 Credits' is a valid value definition" do
      ("pish pish Iron is 3910 Credits" =~ AlienParser::VALUE_DEFINITION).should be_true
    end

    it "'how much is pish tegj glob glob ?' is a valid question" do
      ("how much is pish tegj glob glob ?" =~ AlienParser::QUESTION_RESOLUTION).should be_true
    end

    it "'how many Credits is glob prok Silver ?' is a valid question" do
      ("how many Credits is glob prok Silver ?" =~ AlienParser::QUESTION_RESOLUTION).should be_true
    end

    it "'how many Credits is glob prok Gold ?' is a valid question" do
      ("how many Credits is glob prok Gold ?" =~ AlienParser::QUESTION_RESOLUTION).should be_true
    end

    it "'how many Credits is glob prok Gold ?' is a valid question" do
      ("how many Credits is glob prok Gold ?" =~ AlienParser::QUESTION_RESOLUTION).should be_true
    end

    it "'how much wood could a woodchuck chuck if a woodchuck could chuck wood ?' is a invalid sentence" do
      sentence = "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?"
      (sentence =~ AlienParser::TOKEN_DEFINITION).should be_false
      (sentence =~ AlienParser::VALUE_DEFINITION).should be_false
      (sentence =~ AlienParser::QUESTION_RESOLUTION).should be_false
    end
  end

  describe "Add a sentence 'grok is I' and" do
    before :each do
      @parser = AlienParser.new
      @parser.add_sentence("grok is I")
    end

    it "grok should be 1" do
      @parser.add_sentence("how much is grok ?")
      @parser.evaluate.should == "grok is 1"
    end

    it "grok grok should be 2" do
      @parser.add_sentence("how much is grok grok ?")
      @parser.evaluate.should == "grok grok is 2"
    end

    it "grok grok grok should be 3" do      
      @parser.add_sentence("how much is grok grok grok ?")
      @parser.evaluate.should == "grok grok grok is 3"
    end
    
    it "grok grok grok grok is not a valid sentence" do
      @parser.add_sentence("how much is grok grok grok grok ?")
      @parser.evaluate.should == "I have no idea what you are talking about"
    end

    it "wut is not a valid sentence" do
      @parser.add_sentence("how much is wut ?")
      @parser.evaluate.should == "I have no idea what you are talking about"
    end
  end

  describe "The final boss" do
    before :each do
      @parser = AlienParser.new
      @parser.add_sentence "glob is I"
      @parser.add_sentence "prok is V"
      @parser.add_sentence "pish is X"
      @parser.add_sentence "tegj is L"
      @parser.add_sentence "glob glob Silver is 34 Credits"
      @parser.add_sentence "glob prok Gold is 57800 Credits"
      @parser.add_sentence "pish pish Iron is 3910 Credits"
    end

    it "'pish tegj glob glob' should be 42" do
      @parser.add_sentence "how much is pish tegj glob glob ?"

      @parser.evaluate.should == "pish tegj glob glob is 42"
    end

    it "'glob prok Silver' should be 68 Credits" do
      @parser.add_sentence "how many Credits is glob prok Silver ?"

      @parser.evaluate.should == "glob prok Silver is 68 Credits"
    end

    it "'glob prok Gold' should be 57800 Credits" do
      @parser.add_sentence "how many Credits is glob prok Gold ?"

      @parser.evaluate.should == "glob prok Gold is 57800 Credits"
    end

    it "'glob prok Iron' should be 782 Credits" do
      @parser.add_sentence "how many Credits is glob prok Iron ?"

      @parser.evaluate.should == "glob prok Iron is 782 Credits"
    end

    it "'how much wood could a woodchuck chuck if a woodchuck could chuck wood' cannot be understood" do
      @parser.add_sentence "how much wood could a woodchuck chuck if a woodchuck could chuck wood"

      @parser.evaluate.should == AlienParser::NO_IDEA
    
    end

  end
end